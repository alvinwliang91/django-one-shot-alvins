from django.contrib import admin
from todos.models import TodoList, TodoItem


# Register your models here.
@admin.register(TodoList)
class Todolist(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
        "created_on",
    )


@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
    )
